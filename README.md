# EZ Password Generator

## About

This simple React App generates a password and copies it to your clipboard.
The password has the following characteristics:

- 8 characters long
- 1 random special character
- 1 random number
- 1 random capital letter
- 5 random lower case letters
- special character, capital letter & number are always in a block either at the beginning or the end of the password

## But why?

Like any sane person I use a password manager. However some passwords need to be entered on TV remotes, gamepads and the likes.
In my password manager there is no option to generate a password that has exactly 1 special character, number or upper case letter; only a minimum amount.
Just turning off special characters is not an option since many services require this for passwords.
This causes passwords to look like `yE6u%!Ka` and yeah, have fun entering that with your TV remote or Xbox Controller.
In order to get a reasonably secure password that can still be entered without too much hassle, I created this React App.

Use this at your own risk and definitely don't use this for important services! Use for account for your Smart TV Account that has no credit card connected: OK; Use for Bank account: Not OK
